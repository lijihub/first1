#ifndef __YEJING_H__
#define __YEJING_H__
#include"reg52.h"
#define uint unsigned int
#define uchar unsigned char

sbit rs=P3^5;
sbit rw=P1^1;
sbit e=P3^4;

uchar code tab[]="000.0 cm";
void delay(uint i)
{
	uint m,n;
	for(m=i;m>0;m--)
		for(n=110;n>0;n--);
}

void mingling(uchar zhiling)
{
	rs=0;
	P0=zhiling;
	delay(1);
	e=1;
	delay(1);
	e=0;
}

void shuju(uchar shuju)
{
	rs=1;
	P0=shuju;
	delay(1);
	e=1;
	delay(1);
	e=0;
}

void zhenghe(uint a, uint b)
{
	mingling(0x80+a);
	shuju(0x30+b);
}

void shuru(uint i)
{
	uint bai,shi,ge,xiao,a,b[4]={0,0,0,0};
	for(a=0;i!=0;a++)
	{
		b[a]=i%10;
		i=i/10;
	}
	xiao=b[0];
	ge=b[1];
	shi=b[2];
	bai=b[3];
	zhenghe(4,xiao);
	zhenghe(0,bai);
	zhenghe(1,shi);
	zhenghe(2,ge);
}

void init()
{
	uint a;
	rw=0;
	e=0;
	mingling(0x38);
	mingling(0x0c);
	mingling(0x06);
	mingling(0x01);
	mingling(0x80);
	for(a=0;a<8;a++)
	{
		shuju(tab[a]);
		delay(1);
	}
}
#endif